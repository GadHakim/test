const axios = require('axios');

module.exports = {
    login: async (url, username, password, xApiKey) => {
        return await axios.post('/api/v1/spadm/auth/login', {
            "username": username,
            "password": password,
        }, {
            baseURL: url,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key': xApiKey,
            }
        });
    },
    updateLocalization: async (url, data, accessToken, config) => {
        return await axios.put(`/api/v1/spadm/vendor/${config.index}/translations`, data, {
            baseURL: url,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': accessToken,
                'x-api-key': config.vendor_key,
            }
        });
    },
    getLocalization: async (url, language, config) => {
        return await axios.get(`/api/v1/manapp/translations`, {
            baseURL: url,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Accept-Language': language,
                'x-api-key': config.vendor_key,
            }
        });
    },
};
