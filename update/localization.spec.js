const action = require('../helper/spec.action');
const config = require('../config/config');
const chai = require('chai');
const fs = require('fs');
const path = require('path');
const expect = chai.expect;

const local = {
    arabic: fs.readFileSync(path.join(__dirname, '../lokalise/ikydz/locale', '/ar.json')),
    greek: fs.readFileSync(path.join(__dirname, '../lokalise/ikydz/locale', '/el.json')),
    english: fs.readFileSync(path.join(__dirname, '../lokalise/ikydz/locale', '/en.json')),
    french: fs.readFileSync(path.join(__dirname, '../lokalise/ikydz/locale', '/fr.json')),
    vietnamese: fs.readFileSync(path.join(__dirname, '../lokalise/ikydz/locale', '/vi.json')),
};

const language = {
    arabic: 'ar',
    greek: 'el',
    english: 'en',
    french: 'fr',
    vietnamese: 'vi',
};

async function login(url, config) {
    let response = await action.login(url, config.username, config.password, config.vendor_key);

    expect(response.data.token_type).eql('Bearer');
    expect(response.data.access_token).to.be.a('string');
    expect(response.data.refresh_token).to.be.a('string');

    return 'Bearer ' + response.data.access_token;
}

async function updateLocalization(url, token, config, language, locale) {
    let data = {
        "lang": language,
        "group": "manapp",
        "text": {
            ...JSON.parse(locale)
        }
    };

    await action.updateLocalization(url, data, token, config);
}

async function checkLocalization(url, config, language, locale) {
    let response = await action.getLocalization(url, language, config);
    expect(JSON.parse(locale)).to.eql(response.data);
}

// digifam
// expresso
// ikydz
// kyzpro
// omantel
// pfn
// zain

describe('updated digifam localization [preprod]', () => {
    let url = config.preprod;
    let currentConfig = config.digifam;
    let token = null;

    before('should be login successfully', async () => {
        token = await login(url, config.ikydz);
    });

    it('should be update localization [arabic]', async () => {
        await updateLocalization(url, token, currentConfig, language.arabic, local.arabic);
        await checkLocalization(language.arabic, local.arabic);
    });

    it('should be update localization [greek]', async () => {
        await updateLocalization(url, token, currentConfig, language.greek, local.greek);
        await checkLocalization(url, currentConfig, language.greek, local.greek);
    });

    it('should be update localization [english]', async () => {
        await updateLocalization(url, token, currentConfig, language.english, local.english);
        await checkLocalization(url, currentConfig, language.english, local.english);
    });

    it('should be update localization [french]', async () => {
        await updateLocalization(url, token, currentConfig, language.french, local.french);
        await checkLocalization(url, currentConfig, language.french, local.french);
    });

    it('should be update localization [vietnamese]', async () => {
        await updateLocalization(url, token, currentConfig, language.vietnamese, local.vietnamese);
        await checkLocalization(url, currentConfig, language.vietnamese, local.vietnamese);
    });
});
